package main

import "fmt"

func main() {
	sl := make([]int, 2)

	sl[0] = 90
	sl[1] = 80

	sl = append(sl, 1)

	sl2 := sl[0:2]
	sl2[0] = 200

	fmt.Println(sl)
	fmt.Println(sl2)
	fmt.Println(len(sl))

}