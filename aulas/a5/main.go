package main

import "fmt"

func main() {
	tabuada := [10]int{0, 5, 10}
	user := map[string]string{
		"name": "André",
		"nick": "and",
	}

	slice := []int{0, 5, 10}

	slice = append(slice, 90, 110, 50)

	user["age"] = "36 anos"
	fmt.Println(tabuada)
	fmt.Println(user["name"], user["nick"])//retorna 
	fmt.Println(len(user["age"]))//Tamanho do map

	fmt.Println(slice)

}