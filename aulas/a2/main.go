package main

import "fmt"

func main() {

	number := 2

	if n := 5; n > 0{
		fmt.Println("Maior que 0")
	} else {
		fmt.Println("Menor que 0")
	}

	switch number {
	case 2:
		fmt.Println("Number 2")
	case 5:
		fmt.Println("Number 5")
	default:
		fmt.Println("DEFAULT")
	}
}