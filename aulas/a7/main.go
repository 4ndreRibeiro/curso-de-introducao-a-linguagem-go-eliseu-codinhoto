package main

import "fmt"

func main() {
	arr := [...] int {1,2,3,4}
	user := map[string]string{
		"name": "André",
		"nick": "Ribeiro",
	}

	languages := []string{
		"Go",
		"Python",
		"Java",
		"Nodejs",
		"Scala",
		"Kotlin",
	}

	for key, value := range arr {
		fmt.Println(key, value * value)
	}

	for key, value := range user {
		fmt.Println("O campo \"%s\" tem o valor igual a \"%s\"", key, value)
		
	}

	for _, value := range languages {
		fmt.Println(value)
	}
}